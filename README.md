spaTyper Database documentation
=============

The spaTyper_db is required for running spaTyper. It contains spa sequences used to identify the spa type of the submitted geneome.
Spa sequences in this database are based on the repeat sequences and repeat orders from the spa typing website (<http://www.spaserver.ridom.de/>) that is developed by Ridom GmbH and curated by SeqNet.org (<http://www.SeqNet.org/>).

Installation
=======

Download the spaTyper database by cloning:
```bash
git clone https://git@bitbucket.org/genomicepidemiology/spatyper_db.git
```

Citation
=======

When using this database, please add the following acknowledgement: 
"This publication made use of the spa typing website (<http://www.spaserver.ridom.de/>) that is developed by Ridom GmbH and curated by SeqNet.org (<http://www.SeqNet.org/>)."

and cite

**Harmsen D., Claus H., Witte W., Rothgänger J., Claus H., Turnwald D., & Vogel U.** (2003). Typing of methicillin-resistant Staphylococcus aureus in a university hospital setting using a novel software for spa-repeat determination and database management. J. Clin. Microbiol. **41**:5442-5448

License
=======

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.